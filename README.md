# README #

This documents how to use the RSS feed fixer

### What is this repository for? ###

* Re-writing academic RSS feeds to use a university's EZProxy setup

### How do I get set up? ###

* This project runs as server which proxies specific urls (much like EZproxy itself) and selectively rewrites RSS feeds to change links to be EZProxy URLs
* You will need Python 3.5 and a few libraries (flask) 
* The config.ini files control which feeds/sites are whitelisted for proxying, which links are rewritten within those RSS feeds, which port it runs on, and the ending part of the proxy url
* username and password don't do anything, don't use them
* To run on your remote server, install gunicorn and type `./run_script.sh` (gunicorn is a more stable and sustainable way to run flask apps than serving with python, includes threads etc) 

### Easy start ###

* Navigate to home directory, and type `python3 feed_fixer.py`
* In a browser, go to `http://www.sciencemag.org.feed.localhost:8052/rss/current.xml`
* You should see link urls using ezproxy

### To add a site to config.ini ###

* Add the RSS feed URL to `feed_urls`,
* Add the URL of the the links you want to EZProxy into `host_urls` (usually it's just the host of the RSS feed URL)
* If the URLS aren't reweitten, probably a line needs to be added to the function `rewrite_rss_feed_url` - there turn out to be many ways to specify a link in an RSS feed, and I may not have gotten them all

### To add your new RSS feed to your RSS feed reader of choice ###

* Put the full RSS URL in, including the :port part (just the way you access it in a browser)
* This will require you to have a server running this code somewhere

### Who do I talk to? ###

* Matt