from bs4 import BeautifulSoup
import configparser
import requests
from requests.adapters import HTTPAdapter

MAX_RETRIES = 5
unimelb_login_page = 'https://my.unimelb.edu.au/login/pages/login.jsp'
unimelb_auth_page = 'https://my.unimelb.edu.au/oam/server/auth_cred_submit'
config = configparser.ConfigParser()
config.read('config.ini')
proxy_config = config['proxy_info']

def login_if_needed(url):
    global global_session
    resp = global_session.get(url)
    soup = BeautifulSoup(resp.text, 'html.parser')
    if soup.form is None:
        return
    if soup.form['action'] != unimelb_login_page:
        return
    create_new_session()
    inputs = soup.form.find_all('input')
    request_id = [i['value'] for i in inputs if i['name'] == 'request_id'][0]
    oam_req = [i['value'] for i in inputs if i['name'] == 'OAM_REQ'][0]
    data = {'username': proxy_config['username'],
            'password': proxy_config['password'],
            'request_id': request_id,
            'OAM_REQ': oam_req}
    global_session.post(unimelb_auth_page, data)


def create_new_session():
    global global_session
    adapter = HTTPAdapter(max_retries=MAX_RETRIES)
    global_session = requests.session()
    global_session.mount('https://', adapter)
    global_session.mount('http://', adapter)


if __name__ == '__main__':
    create_new_session()
