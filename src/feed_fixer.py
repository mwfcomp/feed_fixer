from flask import Flask, redirect, request, Response, stream_with_context
import requests
import itertools
import configparser
from urllib.parse import urlparse, urlunparse
from bs4 import BeautifulSoup
import feedparser
import json
import re
import html
import xml.sax.saxutils as sxutils
import cfscrape

config = configparser.ConfigParser()
config.read('config.ini')
proxy_config = config['proxy_info']

feed_urls = json.loads(proxy_config['feed_urls'])
feed_url_hosts = [urlparse(url).hostname for url in feed_urls]

CHUNK_SIZE = 1024
app = Flask(__name__)

proxy_url_end = proxy_config['proxy_url_end']
if proxy_config['port'] != '80':
    proxy_url_end += ':' + proxy_config['port']

scopus_url_end = proxy_config['scopus_url_end']
if proxy_config['port'] != '80':
    scopus_url_end += ':' + proxy_config['port']

scraper = cfscrape.create_scraper()

# make sure to not let ALL sites be proxied
# something that hackers botnets etc autoscan for
@app.route('/<path:path>')
def full_proxy(path):
    if request_path_should_be_proxied(scopus_url_end):
        url = get_real_url(scopus_url_end)
        if is_rss_feed(url):
            return scopus_rewrite_rss_feed_url(url)
        return forward_request_with_headers(url)
    elif request_path_should_be_proxied(proxy_url_end):
        url = get_real_url(proxy_url_end)
        if is_rss_feed(url):
            return rewrite_rss_feed_url(url)
        return forward_request_with_headers(url)
    return 'Url not found'


def is_rss_feed(url):
    return url in feed_urls


def rewrite_url(url):
    parsed = urlparse(url)
    new_host = parsed.netloc + proxy_config['host_suffix']
    if parsed.scheme == 'https':
        scheme = 'http'
    else:
        scheme = parsed.scheme
    return urlunparse((scheme, new_host, parsed.path, parsed.params, parsed.query, parsed.fragment))


def get_rewrite_fn(format_str):
    def rewrite_fn(mo):
        url = mo.group(1)
        url = rewrite_url(url)
        return format_str.format(url)

    return rewrite_fn


def rewrite_item(mo):
    match = re.search('<link>(.*?)</link>', mo.group(0))
    link = match.group(1)
    link = html.unescape(link)
    req = scraper.get(link)
    soup = BeautifulSoup(req.text, 'html.parser')
    abstract_text = soup.find("section", {"id": "abstractSection"}).find("p").text
    abstract_text = sxutils.escape(abstract_text)

    doi = soup.find(id="recordDOI").text
    doi_link = "http://doi.org/" + doi

    def rewrite_desc_to_include_abstract(mo):
        desc = mo.group(1)
        new_desc = '<description>{}{}</description>'.format(desc, abstract_text)
        return new_desc

    def rewrite_link_to_doi_link(mo):
        new_link = '<link>{}</link>'.format(doi_link)
        return new_link

    text = re.sub('<description>(.*?)</description>', rewrite_desc_to_include_abstract, mo.group(0))
    text = re.sub('<link>(.*?)</link>', rewrite_link_to_doi_link, text) #this will later get rewritten to "ezproxy" version

    return text


# pulls the abstract from the notification and the page
def scopus_rewrite_rss_feed_url(feed_url):
    feed = scraper.get(feed_url, params=request.args)
    # feed = requests.get(feed_url, params=request.args)
    text = feed.text
    text = re.sub('<item>(.*?)</item>', rewrite_item, text)
    text = rewrite_rss_text(text)
    resp = Response(text, mimetype=feed.headers['Content-Type'])
    return resp


def rewrite_rss_feed_url(feed_url):
    feed = scraper.get(feed_url, params=request.args)
    text = feed.text
    text = rewrite_rss_text(text)
    resp = Response(text, mimetype=feed.headers['Content-Type'])
    return resp


def rewrite_rss_text(text):
    text = re.sub('<link>(.*?)</link>', get_rewrite_fn('<link>{}</link>'), text)
    text = re.sub('<item rdf:about="(.*?)"', get_rewrite_fn('<item rdf:about="{}"'), text)
    text = re.sub('<rdf:li rdf:resource="(.*?)"', get_rewrite_fn('<rdf:li rdf:resource="{}"'), text)
    text = re.sub('<rdf:li resource="(.*?)"', get_rewrite_fn('<rdf:li resource="{}"'), text)
    text = re.sub('<prism:url>(.*?)</prism:url>', get_rewrite_fn('<prism:url>{}</prism:url>'), text)
    text = re.sub('<feedburner:origLink>(.*?)</feedburner:origLink>',
                  get_rewrite_fn('<feedburner:origLink>{}</feedburner:origLink>'), text)
    text = re.sub('<a href="(.*?)">', get_rewrite_fn('<a href="{}">'), text)
    text = re.sub('<rss:link>(.*?)</rss:link>', get_rewrite_fn('<rss:link>{}</rss:link>'), text)

    return text


def request_path_should_be_proxied(end_of_url):
    base_host, _, _ = request.host_url.rpartition(end_of_url)
    return request.host.endswith(end_of_url) and urlparse(base_host).hostname in feed_url_hosts


def get_real_url(end_of_url):
    host_url, sep, tail = request.host_url.rpartition(end_of_url)
    return host_url + request.path


def stream_raw_response(real_response: requests.models.Response):
    def generate():
        while True:
            chunk = real_response.raw.read(CHUNK_SIZE)
            if not chunk:
                break
            yield chunk

    resp = Response(stream_with_context(generate()))
    resp.headers = dict(real_response.headers)
    return resp


def forward_request_with_headers(url):
    real_response = scraper.get(url, stream=True, params=request.args)
    return stream_raw_response(real_response)


if __name__ == '__main__':
    app.run(port=int(proxy_config['port']), debug=True, host='0.0.0.0')
